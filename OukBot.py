from typing import Optional
import discord
from discord.ext import commands
from googletrans import Translator
import openai
import requests
import json
import string
import random


bot = commands.Bot(command_prefix="/", intents=discord.Intents.all())
# token = os.environ['DISCORD_TOKEN']
token = 'MTA2MjgwMjcwMjIxNzc5MzU2Ng.G-9c3P.h3Bli0UEUxRZjhPVoNZz_xA-jY-Ce-TXOEZ2As'
openai.api_key = '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'

@bot.event
async def on_ready():
    print("Ouk Bot être prêt.")


# --------------------------------------------------------- VARIABLES ---------------------------------------------------------


enabled_users = set()
vowels = "aeiouyAEIOUYàèìòùÀÈÌÒÙáéíóúÁÉÍÓÚâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÄËÏÖÜ"
consonants = "bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZçÇñÑ"
numbers = "0123466789"

transcription_dict = {
    'a': 'Ooh', 'e': 'Ooh', 'o': 'Ooh',
    'i': 'Eek', 'u': 'Eek',
    'b': 'Ouk', 'd': 'Ouk', 'g': 'Ouk', 'p': 'Ouk',
    'c': 'Grrr', 'f': 'Grrr', 'h': 'Grrr', 'j': 'Grrr', 'k': 'Grrr', 'q': 'Grrr', 's': 'Grrr', 'x': 'Grrr', 'z': 'Grrr',
    'l': 'Yow', 'm': 'Yow', 'n': 'Yow', 'r': 'Yow',
    't': 'Wow', 'v': 'Wow',
    'w': 'Hoo hoo', 'y': 'Hoo hoo'
}


# --------------------------------------------------------- HELP & iNTRO ---------------------------------------------------------

#Commande /help_ouk
@bot.command(name='help_ouk', description="Liste des commandes du bot Ouk.\n"
            "Utilisation : /help_ouk <commande>:optionnel")
async def help_ouk(ctx, cmd=None):
    if cmd is None:
        commands = [
            command for command in bot.commands
        ]
        output = 'Liste des commandes: \n'
        for command in commands:
            output += f"{command.name} - {command.description}\n---\n"
        await ctx.send(output)
    else:
        command = bot.get_command(cmd)
        if command is not None:
            await ctx.send(f"{command.name} - {command.description}")
        else:
            await ctx.send(f"La commande {cmd} n'existe pas.")

  

# Le bot répond Ouk lorsqu'on lui dit 'ouk'
@bot.command(name='ouk', description='Ouk')
async def ouk(ctx):
  await ctx.send("Ouk Ouk !")


# --------------------------------------------------------- CRYPT ---------------------------------------------------------


# Transcris le texte de l'utilisateur d'après des règles définies
@bot.command(name='crypt', description="Transcris le texte de l'utilisateur d'après des règles définies.\n"
            "Utilisation : /crypt <message>")
async def crypt(ctx, message: str):
    words = message.split()
    await ctx.message.delete()
    crypted_message = ""
    for word in words:
        word_length = len(word)
        nb_vowels = sum(letter in vowels for letter in word)
        crypted_word = ""
        for letter in word:
            if letter in vowels:
                index = vowels.index(letter)
                crypted_letter = vowels[(index + word_length - nb_vowels) %
                                        len(vowels)]
                crypted_word += crypted_letter
            elif letter in consonants:
                index = consonants.index(letter)
                crypted_letter = consonants[(index - word_length) % len(consonants)]
                crypted_word += crypted_letter
            elif letter in numbers:
                crypted_letter = letter
                crypted_word += crypted_letter
            elif letter in string.punctuation:
                index = string.punctuation.index(letter)
                crypted_letter = string.punctuation[(index + word_length) %
                                                    len(string.punctuation)]
                crypted_word += crypted_letter
            else:
                crypted_word += letter
            crypted_message += crypted_word + " "
    await ctx.send(crypted_message)


# Decrypte le texte de l'utilisateur selon les règles définies
@bot.command(name='decrypt', description="Decrypte le texte de l'utilisateur selon les règles définies.\n"
            "Utilisation : /decrypt <message>")
async def decrypt(ctx, message: str):
    words = message.split()
    await ctx.message.delete()
    decrypted_message = ""
    for word in words:
        word_length = len(word)
        nb_vowels = sum(letter in vowels for letter in word)
        decrypted_word = ""
        for letter in word:
            if letter in vowels:
                index = vowels.index(letter)
                decrypted_letter = vowels[(index - word_length + nb_vowels) %
                                        len(vowels)]
                decrypted_word += decrypted_letter
            elif letter in consonants:
                index = consonants.index(letter)
                decrypted_letter = consonants[(index + word_length) % len(consonants)]
                decrypted_word += decrypted_letter
            elif letter in numbers:
                decrypted_letter = letter
                decrypted_word += decrypted_letter
            elif letter in string.punctuation:
                index = string.punctuation.index(letter)
                decrypted_letter = string.punctuation[(index - word_length) %
                                                    len(string.punctuation)]
                decrypted_word += decrypted_letter
            else:
                decrypted_word += letter
            decrypted_message += decrypted_word + " "
    await ctx.author.send(decrypted_message)
    

# --------------------------------------------------------- TRADUCTION ---------------------------------------------------------

    
# # Ajoute l'utilisateur à la whitelist
# @bot.command(name='toulon_by_night', description='Ajoute l\'utilisateur à la whitelist.')
# async def toulon_by_night(ctx):
#   enabled_users.add(ctx.author.id)
#   message = await ctx.author.send(
#     'Toi être des notres Kirikou. Si toi vouloir découvrir language divin, toi taper "/simien" suivi de ton message entre "guillemet".'
#   )

# Convertit le texte de l'utilisateur depuis l'alphabet Simien
@bot.command(name='simien', description="Convertit le texte de l'utilisateur depuis l'alphabet Simien.\n"
            "Utilisation : /simien <message>")
async def simien(ctx, *, message: str):
    words = message.split()
    transcribed_message = ""
    for word in words:
        transcribed_word = ""
        for letter in word:
            if letter in transcription_dict:
                if random.random() < 0.6:
                    transcribed_word += transcription_dict[letter]
                else:
                    transcribed_word += transcription_dict[letter] + " "
            elif letter in numbers:
                transcribed_word += ""
            elif letter in string.punctuation:
                transcribed_word += ""
            else:
                transcribed_word += ""
            transcribed_message += transcribed_word + " "
    await ctx.send(transcribed_message)


# Traduit le texte de l\'utilisateur en arabe
@bot.command(name='arabe', description="Traduit le texte de l'utilisateur en Arabe.\n"
             "Utilisation : /arabe <message>")
async def arabe(ctx, message: str):
    if not message:
        await ctx.send("Veuillez entrer un message à traduire après la commande \"/arabe\".")
    else:
        message = message.replace('"', '')
        url = 'https://translation.googleapis.com/language/translate/v2'
        data = {'q': message, 'target': 'ar'}
        headers = {'X-Goog-Api-Key': 'AIzaSyA9rfb9wEpS4ZTCLJLpUfACKqECmluNJac'}
        response = requests.post(url, data=data, headers=headers)
        json_response = response.json()
        translated_text = json_response['data']['translations'][0]['translatedText']
        await ctx.send(translated_text)
        
        
# Traduit le texte de l\'utilisateur en fr
@bot.command(name='gaule', description="Traduit le texte de l'utilisateur en Français.\n"
             "Utilisation : /gaule <message>")
async def gaule(ctx, message: str):
    if not message:
        await ctx.send("Veuillez entrer un message à traduire après la commande \"/francais\".")
    else:
        message = message.replace('"', '')
        url = 'https://translation.googleapis.com/language/translate/v2'
        data = {'q': message, 'target': 'fr'}
        headers = {'X-Goog-Api-Key': 'AIzaSyA9rfb9wEpS4ZTCLJLpUfACKqECmluNJac'}
        response = requests.post(url, data=data, headers=headers)
        json_response = response.json()
        translated_text = json_response['data']['translations'][0]['translatedText']
        translated_text = translated_text.replace('&#39;', "'")
        await ctx.send(translated_text)
    

# Traduit le texte de l\'utilisateur en Klingon
@bot.command(name='klingon', description="Traduit le texte de l'utilisateur en Klingon.\n"
             "Utilisation : /klingon <message>")
async def klingon(ctx, *, message: str):
    to_translate = 'traduis "' + message + '" en anglais'
    response = openai.Completion.create(engine="text-curie-001", prompt=to_translate)
    url = 'https://api.funtranslations.com/translate/klingon.json'
    data = {'text': response.choices[0].text}
    response = requests.post(url, json=data)
    json_data = json.loads(response.text)
    klingon_text = json_data['contents']['translated']
    await ctx.send(klingon_text)


# --------------------------------------------------------- SORCELLERIE ---------------------------------------------------------


# Affiche le verset demandé de la sourate
@bot.command(name='sourate', description="Affiche le verset demandé de la sourate.\n"
             "Utilisation : /sourate <sourate> <verset>:optionnel")
async def sourate(ctx, numero_sourate: int, numero_verset: Optional[int]):
    url = f"https://api.alquran.cloud/v1/ayah/{numero_sourate}:{numero_verset}"
    result = requests.get(url).json()
    ayah = result["data"]
    await ctx.send(f"Verset {ayah['numberInSurah']} de la sourate {ayah['surah']['name']} : {ayah['text']}")

# Affiche le verset demandé du livre renseigné
@bot.command(name='gonin', description="Affiche le verset demandé du livre renseigné.\n"
             "Utilisation : /gonin <livre> <verset>:optionnel")
async def gonin(ctx, numero_livre: int, numero_verset: Optional[int]):
    livres = ["Genesis","Exodus","Leviticus","Numbers","Deuteronomy"]
    url = "https://bible-api.com/{}+{}:{}".format(livres[numero_livre-1], numero_livre, numero_verset)
    res = requests.get(url).json()
    verset = res['text']
    await ctx.send(verset)


# --------------------------------------------------------- OWN CHAT GPT ---------------------------------------------------------


# Utilise chat GPT pour effectuer un prompt
@bot.command(name='gpt', description="Utilise chat GPT pour effectuer un prompt.\n"
             "Utilisation : /gpt <message> <token>:optionnel")
async def gpt(ctx, message: str, mtoken: Optional[int] = 150):
    if not message:
        raise commands.BadArgument(param='Moi pas devin, connard. Toi rajouter phrase entre "guillemets" après commande "/gpt".')
    response = openai.Completion.create(engine="text-curie-001", prompt=message, temperature=0.9, max_tokens=mtoken, top_p=1, frequency_penalty=0, presence_penalty=0.6)
    await ctx.send(response.choices[0].text)


# @bot.event
# async def on_message(message):
#   if message.author == bot.user:  # pour ignorer les messages envoyés par le bot
#     return
#   if message.guild is None:  # pour vérifier si le message est privé ou non
#     response = openai.Completion.create(engine="text-curie-001", prompt=message.content, temperature=0.9, max_tokens=150, top_p=1, frequency_penalty=0, presence_penalty=0.6)
#     await message.channel.send(response.choices[0].text)


bot.run(token)
